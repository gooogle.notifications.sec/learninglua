--[[
	implementacion del metodo de biparticion o biseccion.
	Consiste en aplicar el teorema del valor medio que dice
	que en una funcion continua entre a y b, si f(a)*f(b) es
	menor que 0, entonces existe una raiz entre esas dos
	cotas, mediante el metodo de biseccion se aplica con un
	metodo iterativo una seleccion inteligente del punto medio 
	de modo que acaba convergiendo en el resultado
]]

local b = {
	_TYPE = 'function',
	_VERSION = '0.1.1.20220329',
	_NAME = 'bipart'
}

local function bipart (fcn,a,b,N)
	if N == nil then
		N = 20
	end
	local c = (a+b)/2
	for i = 1,N,1 do
		if fcn(a) * fcn(c) < 0 then
			b = c
		elseif fcn(a) * fcn(c) > 0 then
			a = c
		else
			return c, i
		end
		c = (a+b)/2
		i = i + 1
	end
	return c,N
end 

b.bipart = bipart
return b

