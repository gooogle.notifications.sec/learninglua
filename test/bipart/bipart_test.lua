local bipart = require "bipart".bipart
local math = require "math"


local function f1(x)
	return 2*x - 1.23
end

local function f2(x)
	return 6.5*x + 2*math.cos(3*x) - 1
end

local result, iter = bipart(f1,0,1)
print("result: "..tostring(result) , "iterations: "..tostring(iter),"error: " .. tostring(f1(result)))
local result, iter = bipart(f2,-3,2,40)
print("result: "..tostring(result) , "iterations: "..tostring(iter),"error: " .. tostring(f2(result)))

