local filter = require "tabext".filter
local reduce = require "tabext".reduce
local map = require "tabext".map

local t = {1,2,3,4,5,6,7,8,9,10,11,12,13,14}
local r_test = filter(t,function (e) return e > 7 end)
local r_test_correct = {8,9,10,11,12,13,14}
local ok = true

for i,v in ipairs(r_test) do
	if v ~= r_test_correct[i] then
		print("error on test 1")
		ok = false
		break
	end
end
if ok then
	print("test 1 ok")
end

t = {
	{
		x = 10,
		y = 12
	},
	{
		x = -19,
		y = 192
	},
	{
		x = 0.78,
		y = 1.22
	},
	{
		x = 67,
		y = 778
	}
}

r_test = map(t,function (e) return {x = e.x, y = e.y, z = e.x + e.y} end)
r_test_correct = {
	{
		x = 10,
		y = 12,
		z = 22
	},{
		x = -19,
		y = 192,
		z = 173
	},{
		x = 0.78,
		y = 1.22,
		z = 2.0
	},{
		x = 67,
		y = 778,
		z = 845
	}
}
ok = true
for i,v in ipairs(r_test) do
	if (v.x ~= r_test_correct[i].x or v.y ~= r_test_correct[i].y or v.z ~= r_test_correct[i].z) then
		print("error on test 2")
		ok = false
		break
	end
end
if ok then print("test 2 ok") end



t = {1,2,3,4,5,6,7,8}
r_test = reduce(t,function (acum,e) 
	if acum ~= nil then 
		return acum + e
	end
	return e
end)
r_test_correct = 36
ok = true
if r_test_correct ~= r_test then
	print("error on test 3")
	ok = false 
end
if ok then
	print("test 3 ok")
end
