--[[
-- lua example implementing filter, map and reduce methods
--]]

--[[
-- filter takes 2 arguments, the first is the target table
-- and the second is a funcion that takes 2 parameters, the 
-- first parameter is the element that is processed and the 
-- second parameter is it index
-- when fcn returns true, the element is attached to the 
-- returned table
--]]
function _filter (t,fcn)
	local ret = {}
	for i,e in ipairs(t) do
		if fcn(e,i) then
			table.insert(ret,e)
		end
	end
	return ret
end
--[[
-- reduce takes 2 arguments, the first is the target table 
-- and the second is the evaluation function, this function 
-- takes 3 parameters, the first parameter is the acumulator
-- , the second is the element and the third is the index
--]]
function _reduce (t,fcn)
	local acum =nil
	for i,e in ipairs(t) do
		acum = fcn(acum,e,i)
	end
	return acum
end
--[[
-- map get the table t and the funcion fcn and fcn has the
-- responsability of return values by mapping the target element
-- e and the index i
--]]
function _map (t,fcn)
	local ret = {}
	for i,e in ipairs(t) do
		table.insert(ret,fcn(e,i))
	end
	return ret
end

return {
	filter = _filter,
	reduce = _reduce,
	map = _map
}
