# Lua learning repo

![lua logo](https://www.logolynx.com/images/logolynx/ba/ba0fc6fdf38d5e54279d4aa33172f09e.jpeg)


### Objectives of this repo

This site was made for all those exercises, tests, examples, practices or some miscelaneous 
functions in Lua programming language.

There's no rules of the files that you can post except: 
	* potential malware
	* other programming languaje code
	* religious, politics, nfsw ot any kind of harmful content

### How to contribute?
```
If you want to share your request, please send your pull request and then the repository owner
will include it to the master.
Please before do pull request, write into the CONTRIBUTORS file your username.
```

### Filesystem description

```
- other: other programming languajes scripts
- examples: code examples in Lua
- test: test code
- misc: miscelaneous functionalities, e.g. create a function that reads csv files 
- Love2d: LOVE2D framework code (please englobe it into a folder with the project name)
- CONTRIBUTORS: contributors index file
- README.md: this file
- COPYING: license of the repo (GPL3)
- INDEX: index tree (before any pull request, please consider do ( tree > INDEX )
- NOTES: notes file, if you want to write any kind of information
```
